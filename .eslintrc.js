module.exports = {
  'env': {
    'browser': true,
    'es6': true,
  },
  'extends': [
    'plugin:vue/essential',
    'google',
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
  },
  'parserOptions': {
    'ecmaVersion': 2018,
    'sourceType': 'module',
  },
  'plugins': [
    "html",
    'vue',
  ],
  'rules': {
    "linebreak-style": 0,
    "object-curly-spacing": 0,
    quotes: ["error", "single"],
    "comma-dangle": 0,
    "arrow-parens": 0
  },
};
