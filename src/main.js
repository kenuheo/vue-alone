import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import App from './App.vue';
import VueRouter from 'vue-router';
import Home from './components/Home.vue';
import Register from './components/Register.vue';
import Login from './components/Login.vue';
import HelloWorld from './components/HelloWorld.vue';
import UserList from './components/users/UserList.vue';
import UserDetail from './components/users/UserDetail.vue';
import UserForm from './components/users/UserForm.vue';
import UserUpdate from './components/users/UserUpdate.vue';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

const routes = [
  { path: '/', component: Home },
  { path: '/register', component: Register },
  { path: '/login', component: Login },
  { path: '/hello', component: HelloWorld },
  { path: '/users', component: UserList },
  { path: '/users/user_form', component: UserForm },
  { path: '/users/user_update/:id', component: UserUpdate },
  { path: '/users/:id', component: UserDetail }
];

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VueRouter);

const router = new VueRouter({ routes });

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
